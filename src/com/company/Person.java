package com.company;
import  java.util.*;
class Person {
    private static Person person = null;
    private Vector list = new Vector();


    static Person getInstance(){
         if(person == null){
             person = new Person();
         }
         return person;
    }

    void addPerson(String person){
        if(!list.contains(person)){
            list.add(person);
            System.out.println("Person successfully added");
        }else {
            System.out.println("That pearson already exist!");
        }
    }
    void showAllPersons(){
        System.out.println("Person list:");
        for (Object aList : list) {
            System.out.println((String) aList);
        }
    }
    void checkPearson(String person){
        if(list.contains(person)){
            System.out.println("That person exist!");
        }else {
            System.out.println("that person doesn't exist!");
        }
    }
    void removePerson(String pearson){
        if(list.contains(pearson)){
            list.remove(pearson);
            System.out.println("Person successfully removed!");
        }else {
            System.out.println("that person doesn't exist!");
        }
    }
}
