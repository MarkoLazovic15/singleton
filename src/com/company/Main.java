package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while (true) {
            Scanner scanner = new Scanner(System.in);
            Person person = Person.getInstance();
            System.out.println("\nChose option: ");
            System.out.println("1.Add person.");
            System.out.println("2.Show all persons.");
            System.out.println("3.Check person.");
            System.out.println("4.remove person.");
            System.out.println("5.Exit");
            int option = scanner.nextInt();
            scanner = new Scanner(System.in);
            switch (option) {
                case 1: {
                    System.out.println("Enter person:");
                    person.addPerson(scanner.nextLine());
                    break;
                }
                case 2: {
                    person.showAllPersons();
                    break;
                }
                case 3: {
                    System.out.println("Enter person:");
                    person.checkPearson(scanner.nextLine());
                    break;
                }
                case 4: {
                    System.out.println("Enter person:");
                    person.removePerson(scanner.nextLine());
                    break;
                }case 5:{
                    System.exit(0);
                }
                default: {
                    System.out.println("Wrong option!");
                }
            }
        }
    }
}
